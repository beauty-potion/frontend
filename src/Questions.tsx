import { TypesDePeau } from "@beauty-potion/api";
import QuestionnaireSingleChoice from "./components/questionnaire/QuestionnaireSingleChoice";
import { z } from "zod";
import { GetRoutinesParametersType } from "./types/ApiTypes";

const questions = [
  <QuestionnaireSingleChoice<
    GetRoutinesParametersType,
    "typeDePeau",
    z.infer<typeof TypesDePeau>
  >
    field="typeDePeau"
    key="type de peau"
    question="Quel est votre type de peau?"
    choices={[
      {
        value: TypesDePeau.Enum.seche,
        title: "Séche",
        subtitle: "La peau tiraille et/ou péle",
      },
      {
        value: TypesDePeau.Enum.grasse,
        title: "Grasse",
        subtitle: "La peau brille",
      },
      {
        value: TypesDePeau.Enum.mixte,
        title: "Mixte",
        subtitle:
          "La peau est grasse uniquement sur le front, le nez et le menton",
      },
      {
        value: TypesDePeau.Enum.normale,
        title: "Normale",
        subtitle: "La peau n’est ni séche ni grasse",
      },
    ]}
  />,
];

export default questions;
