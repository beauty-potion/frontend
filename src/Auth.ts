import Router from "next/router";
import SuperTokens from "supertokens-auth-react";
import Session from "supertokens-auth-react/recipe/session/index.js";
import ThirdPartyEmailPassword from "supertokens-auth-react/recipe/thirdpartyemailpassword/index.js";

function Init() {
  SuperTokens.init({
    appInfo: {
      websiteDomain: process.env.NEXT_WEBSITE_URL ?? "localhost:8000",
      apiDomain: process.env.NEXT_PUBLIC_API_URL ?? "localhost:3000",
      apiBasePath: "/auth",
      appName: "Beauty Potion",
    },
    recipeList: [Session.init(), ThirdPartyEmailPassword.init()],
    windowHandler: (oI: any) => {
      return {
        ...oI,
        location: {
          ...oI.location,
          setHref: (href: string) => {
            Router.push(href);
          },
        },
      };
    },
  });
}

export { Init };
