import { AuthContext, AuthContextType } from "@/providers/AuthProvider";
import { useContext } from "react";

const useAuth = () => useContext(AuthContext) as AuthContextType;

export default useAuth;
