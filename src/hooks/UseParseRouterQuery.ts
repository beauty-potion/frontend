import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { ZodSchema, z } from "zod";

type UseParseRouterQueryProps<SchemaType extends ZodSchema> = {
  schema: SchemaType;
};
const useParseRouterQuery = <SchemaType extends ZodSchema>({
  schema,
}: UseParseRouterQueryProps<SchemaType>): [z.infer<SchemaType>, boolean] => {
  const { query, isReady } = useRouter();
  const [parsedQuery, setParsedQuery] = useState<z.infer<SchemaType>>();
  const [parsingFailed, setParsingFailed] = useState(false);

  //here we can't simply use a useMemo because isReady is always false server-side at first render but not client-side
  //which would cause a hydration error as any conditionnal render based on this would differ between server and client
  useEffect(() => {
    if (isReady) {
      const parsingResult = schema.safeParse(query);
      if (parsingResult.success) setParsedQuery(parsingResult.data);
      else setParsingFailed(true);
    }
  }, [isReady, query, schema]);

  return [parsedQuery, parsingFailed];
};

export default useParseRouterQuery;
