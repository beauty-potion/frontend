import { QueryKey, useQuery, UseQueryOptions } from "@tanstack/react-query";

function UseBaseQuery<
  TQueryFnData = unknown,
  TData = TQueryFnData,
  TQueryKey extends QueryKey = QueryKey
>(queryParameters: UseQueryOptions<TQueryFnData, Error, TData, TQueryKey>) {
  const query = useQuery(queryParameters);
  if (query.error) console.error(queryParameters.queryFn, query.error);
  return query;
}

export default UseBaseQuery;
