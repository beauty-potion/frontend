import api from "@/Api";
import { GetRoutinesParametersType } from "@/types/ApiTypes";
import UseBaseQuery from "./UseBaseQuery";

const UseGetRoutinesQuery = (params?: GetRoutinesParametersType) => {
  return UseBaseQuery({
    queryKey: ["getRoutines"],
    queryFn: !!params ? () => api.getRoutines(params) : () => null,
    enabled: !!params,
  });
};

export default UseGetRoutinesQuery;
