import api from "@/Api";
import {
  LoginType,
  SignInResponse,
  SignUpResponse,
} from "@/schemas/LoginSchema";
import { useRouter } from "next/router";
import { createContext, PropsWithChildren, useEffect, useState } from "react";
import {
  doesSessionExist,
  getAccessToken,
  getAccessTokenPayloadSecurely,
} from "supertokens-auth-react/recipe/session";
import {
  doesEmailExist,
  emailPasswordSignIn,
  emailPasswordSignUp,
  signOut,
} from "supertokens-auth-react/recipe/thirdpartyemailpassword";

type User = {
  email: string;
};

type AuthContextType = {
  user: User | null;
  signUp: (login: LoginType) => Promise<SignUpResponse>;
  signIn: (login: LoginType) => Promise<SignInResponse>;
  signOut: () => Promise<void>;
  continueToLastAttemptedRoute: () => void;
};

const AuthContext = createContext<AuthContextType | undefined>(undefined);

const authorizedRoutes = ["/", "/login"];

const AuthProvider = ({ children }: PropsWithChildren) => {
  const router = useRouter();
  const [lastAttemptedRoute, setLastAttemptedRoute] = useState<string>();
  const [user, setUser] = useState<User | null>(null);
  const [isSessionLoading, setIsSessionLoading] = useState(true);
  const isAuthorized = !!user || authorizedRoutes.includes(router.pathname);

  useEffect(() => {
    if (!isSessionLoading && !isAuthorized) {
      router.push("/login");
      setLastAttemptedRoute(router.asPath);
    }
  }, [isAuthorized, isSessionLoading]);

  useEffect(() => {
    doesSessionExist().then((exist) => {
      if (exist) setUserAndToken();
      setIsSessionLoading(false);
    });
  }, []);

  function continueToLastAttemptedRoute() {
    setLastAttemptedRoute(undefined);
    router.push(lastAttemptedRoute ?? "/");
  }

  async function setUserAndToken() {
    const { email } = await getAccessTokenPayloadSecurely();
    setUser({ email });
    const accessToken = await getAccessToken();
    if (!accessToken)
      throw new Error(
        "Trying to set API token but supertokens session acessToken is undefined"
      );
    api.setAuthToken(accessToken);
  }

  async function signUp({ email, password }: LoginType) {
    try {
      const { doesExist } = await doesEmailExist({ email });
      if (doesExist) return SignUpResponse.emailAlreadyExists;
      let { status } = await emailPasswordSignUp({
        formFields: [
          {
            id: "email",
            value: email,
          },
          {
            id: "password",
            value: password,
          },
        ],
      });
      await setUserAndToken();
      return status == "OK"
        ? SignUpResponse.success
        : SignUpResponse.unknownError;
    } catch {
      return SignUpResponse.unknownError;
    }
  }

  async function signIn({ email, password }: LoginType) {
    try {
      let { status } = await emailPasswordSignIn({
        formFields: [
          {
            id: "email",
            value: email,
          },
          {
            id: "password",
            value: password,
          },
        ],
      });
      if (status == "WRONG_CREDENTIALS_ERROR")
        return SignInResponse.wrongEmailOrPassword;
      await setUserAndToken();
      return status == "OK"
        ? SignInResponse.success
        : SignInResponse.unknownError;
    } catch {
      return SignInResponse.unknownError;
    }
  }

  return (
    <AuthContext.Provider
      value={{
        user,
        signUp,
        signIn,
        signOut,
        continueToLastAttemptedRoute,
      }}
    >
      {isAuthorized && !isSessionLoading && children}
    </AuthContext.Provider>
  );
};

export { AuthContext, AuthProvider };
export type { AuthContextType, User };
