import { Init as InitAuth } from "@/Auth";
import { AuthProvider } from "@/providers/AuthProvider";
import QueriesProvider from "@/providers/QueriesProvider";
import theme from "@/theme";
import { ChakraProvider } from "@chakra-ui/react";
import type { AppProps } from "next/app";
import { SuperTokensWrapper } from "supertokens-auth-react";
import { GoogleAnalytics } from "@next/third-parties/google";

if (typeof window !== "undefined") {
  InitAuth();
}

export default function App({ Component, pageProps }: AppProps) {
  return (
    <>
      {process.env.NEXT_PUBLIC_GOOGLE_ANALYTICS_ID && (
        <GoogleAnalytics gaId={process.env.NEXT_PUBLIC_GOOGLE_ANALYTICS_ID} />
      )}
      <ChakraProvider theme={theme}>
        <SuperTokensWrapper>
          <AuthProvider>
            <QueriesProvider>
              <Component {...pageProps} />
            </QueriesProvider>
          </AuthProvider>
        </SuperTokensWrapper>
      </ChakraProvider>
    </>
  );
}
