import Page from "@/components/Page";
import { Button, Center, Heading, VStack } from "@chakra-ui/react";
import NextLink from "next/link";

const Home = () => (
  <Page backgroundColor="secondary.500">
    <Center height="70vh" width="100%">
      <VStack gap={10}>
        <Heading textAlign="center">
          Concoctez vos routines de skincare selon vos besoins et vos critères{" "}
        </Heading>
        <NextLink href="/questionnaire" passHref>
          <Button colorScheme="primary" size="lg">
            Commencer
          </Button>
        </NextLink>
      </VStack>
    </Center>
  </Page>
);

export default Home;
