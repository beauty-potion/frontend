import LoginForm from "@/components/login/LoginForm";
import SignupForm from "@/components/login/SignupForm";
import Page from "@/components/Page";
import useAuth from "@/hooks/UseAuth";
import { Tab, TabList, TabPanel, TabPanels, Tabs } from "@chakra-ui/react";
import { useEffect } from "react";

const Login = () => {
  const { signIn, signUp, user, continueToLastAttemptedRoute } = useAuth();
  const isAlreadyAuthenticated = !!user;

  useEffect(() => {
    if (isAlreadyAuthenticated) continueToLastAttemptedRoute();
  }, [isAlreadyAuthenticated]);

  return (
    <Page>
      {!isAlreadyAuthenticated && (
        <Tabs isFitted colorScheme="primary">
          <TabList>
            <Tab>Se connecter</Tab>
            <Tab>S&apos;inscire</Tab>
          </TabList>
          <TabPanels>
            <TabPanel>
              <LoginForm onSubmit={signIn} />
            </TabPanel>
            <TabPanel>
              <SignupForm onSubmit={signUp} />
            </TabPanel>
          </TabPanels>
        </Tabs>
      )}
    </Page>
  );
};

export default Login;
