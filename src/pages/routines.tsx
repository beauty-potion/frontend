import ErrorAlert from "@/components/ErrorAlert";
import Page from "@/components/Page";
import ProduitsView from "@/components/routines/ProduitsView";
import RoutinesView from "@/components/routines/RoutinesView";
import ViewSwitcher, { View } from "@/components/routines/ViewSwitcher";
import useParseRouterQuery from "@/hooks/UseParseRouterQuery";
import UseGetRoutinesQuery from "@/queries/UseGetRoutinesQuery";
import { GetRoutinesParameters } from "@beauty-potion/api/dist/schemas/GetRoutines";
import { Center, Spinner } from "@chakra-ui/react";
import { useState } from "react";

const RoutinesPage = () => {
  const [routinesParams, routineParamsError] = useParseRouterQuery({
    schema: GetRoutinesParameters,
  });

  const {
    data: routines,
    error: queryError,
    isLoading: queryIsLoading,
  } = UseGetRoutinesQuery(routinesParams);

  const [currentView, setCurrentView] = useState<View>("routines");

  const PageContent = () => {
    if (routineParamsError || queryError) return <ErrorAlert />;
    else if (queryIsLoading) return <Spinner />;
    else if (routines)
      return (
        <>
          <Center my={5}>
            <ViewSwitcher
              value={currentView}
              onChange={(view) => setCurrentView(view)}
            />
          </Center>
          {currentView == "routines" ? (
            <RoutinesView matin={routines} />
          ) : (
            <ProduitsView />
          )}
        </>
      );
  };

  return (
    <Page>
      <PageContent />
    </Page>
  );
};

export default RoutinesPage;
