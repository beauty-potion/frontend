import Page from "@/components/Page";
import Questionnaire from "@/components/questionnaire/Questionnaire";
import questions from "@/Questions";
import { GetRoutinesParametersType } from "@/types/ApiTypes";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";

const RoutinesPreferencesQuestionnaire =
  Questionnaire<GetRoutinesParametersType>;

const QuestionnairePage = () => {
  const [currentStepIndex, setCurrentStepIndex] = useState(0);
  const [routinesChoices, setRoutinesChoices] = useState<
    Partial<GetRoutinesParametersType>
  >({});
  const [isCompleted, setIsCompleted] = useState(false);
  const router = useRouter();

  useEffect(() => {
    if (isCompleted) goToRoutines();
  }, [isCompleted]);

  function handleQuestionnaireChanged(
    fields: Partial<GetRoutinesParametersType>
  ) {
    setRoutinesChoices((previous) => ({
      ...previous,
      ...fields,
    }));
    if (currentStepIndex < questions.length - 1) goNextStep();
    else setIsCompleted(true);
  }

  function goToRoutines() {
    router.push({ pathname: "routines", query: routinesChoices });
  }

  function goNextStep() {
    setCurrentStepIndex((previous) =>
      Math.min(questions.length - 1, previous + 1)
    );
  }

  return (
    <Page backgroundColor="secondary.500">
      <RoutinesPreferencesQuestionnaire
        currentStepIndex={currentStepIndex}
        steps={questions}
        currentValue={routinesChoices}
        onChange={handleQuestionnaireChanged}
      />
    </Page>
  );
};

export default QuestionnairePage;
