import { extendTheme } from "@chakra-ui/react";

const theme = extendTheme({
  colors: {
    primary: {
      50: "#FEF9F9",
      100: "#FCEEEF",
      200: "#F9D7DA",
      300: "#F6C1C6",
      400: "#F2ABB1",
      500: "#EF959D",
      600: "#EC838D",
      700: "#EA727C",
      800: "#E7606C",
      900: "#E44E5B",
      950: "#E34553",
    },
    secondary: {
      50: "#E0F0FF",
      100: "#D8ECFF",
      200: "#C9E5FF",
      300: "#BADEFF",
      400: "#AAD6FF",
      500: "#9BCFFF",
      600: "#82C3FF",
      700: "#68B7FF",
      800: "#4FAAFF",
      900: "#359EFF",
      950: "#2898FF",
    },
    sucess: {
      50: "#F3FDF3",
      100: "#EAFCEB",
      200: "#D8FAD9",
      300: "#C6F7C8",
      400: "#B4F5B7",
      500: "#A2F2A6",
      600: "#90EF95",
      700: "#7EED84",
      800: "#6CEA73",
      900: "#5AE861",
      950: "#51E759",
    },
  },
  textStyles: {
    comment: {
      color: "#7A7A7A",
    },
  },
});

export default theme;
