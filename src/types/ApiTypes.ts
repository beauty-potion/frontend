import { GetRoutinesParameters } from "@beauty-potion/api/dist/schemas/GetRoutines";
import { z } from "zod";

type GetRoutinesParametersType = z.infer<typeof GetRoutinesParameters>;
export type { GetRoutinesParametersType };
