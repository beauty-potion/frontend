import {
  Alert,
  AlertDescription,
  AlertIcon,
  Button,
  FormControl,
  FormErrorMessage,
  FormHelperText,
  Input,
  Stack,
} from "@chakra-ui/react";
import { zodResolver } from "@hookform/resolvers/zod";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { z } from "zod";
import PasswordInput from "./PasswordInput";
import { LoginSchema, LoginType, SignUpResponse } from "@/schemas/LoginSchema";

type SignupFormProps = {
  onSubmit: (data: LoginType) => Promise<SignUpResponse>;
};

const SignupWithConfirmationPasswordSchema = z
  .intersection(
    LoginSchema,
    z.object({
      confirmationPassword: z.string(),
    })
  )
  .refine(
    (data) => {
      return data.password == data.confirmationPassword;
    },
    { path: ["confirmationPassword"] }
  );

const SignupForm = ({ onSubmit }: SignupFormProps) => {
  const {
    handleSubmit,
    register,
    formState: { errors },
  } = useForm<z.infer<typeof SignupWithConfirmationPasswordSchema>>({
    resolver: zodResolver(SignupWithConfirmationPasswordSchema),
  });

  const [isSubmiting, setIsSubmiting] = useState(false);
  const [emailAlreadyExists, setEmailAlreadyExists] = useState(false);
  const [unknownErrorHappened, setUnknownErrorHappened] = useState(false);

  const passwordRulesMessage =
    "Doit contenir 8 caractères, une minuscule, une majuscule et un chiffre.";

  let emailErrorMessage = undefined;
  if (!!errors.email) emailErrorMessage = "Email invalide";
  else if (emailAlreadyExists) emailErrorMessage = "Cet email est déjà utilisé";

  async function submit(data: LoginType) {
    setIsSubmiting(true);
    setEmailAlreadyExists(false);
    const response = await onSubmit(data);
    setEmailAlreadyExists(response == SignUpResponse.emailAlreadyExists);
    setUnknownErrorHappened(response == SignUpResponse.unknownError);
    setIsSubmiting(false);
  }

  return (
    <form onSubmit={handleSubmit(submit)}>
      <Stack>
        <FormControl isInvalid={!!emailErrorMessage}>
          <Input {...register("email")} placeholder="Email" />
          <FormErrorMessage>{emailErrorMessage}</FormErrorMessage>
        </FormControl>
        <Stack direction="row">
          <FormControl isInvalid={!!errors.password}>
            <PasswordInput
              {...register("password")}
              placeholder="Mot de passe"
            />
            {!errors.password ? (
              <FormHelperText>{passwordRulesMessage}</FormHelperText>
            ) : (
              <FormErrorMessage>{passwordRulesMessage}</FormErrorMessage>
            )}
          </FormControl>
          <FormControl isInvalid={!!errors.confirmationPassword}>
            <PasswordInput
              {...register("confirmationPassword")}
              placeholder="Confirmation de mot de passe"
            />
            <FormErrorMessage>
              Les mots de passe doivent correspondrent
            </FormErrorMessage>
          </FormControl>
        </Stack>
        <Button type="submit" colorScheme="primary" isLoading={isSubmiting}>
          S&apos;inscrire
        </Button>
        {unknownErrorHappened && (
          <Alert status="error">
            <AlertIcon />
            <AlertDescription>
              Une erreur est survenue, merci de réessayer plus tard
            </AlertDescription>
          </Alert>
        )}
      </Stack>
    </form>
  );
};

export default SignupForm;
