import { ViewIcon, ViewOffIcon } from "@chakra-ui/icons";
import {
  IconButton,
  Input,
  InputGroup,
  InputProps,
  InputRightElement,
} from "@chakra-ui/react";
import { forwardRef, useState } from "react";

const PasswordInput = forwardRef((inputProps: InputProps, ref) => {
  const [isVisible, setIsVisible] = useState(false);

  return (
    <InputGroup size="md">
      <Input {...inputProps} ref={ref} type={isVisible ? "text" : "password"} />
      <InputRightElement width="4.5rem">
        <IconButton
          icon={isVisible ? <ViewOffIcon /> : <ViewIcon />}
          h="1.75rem"
          size="sm"
          onClick={() => setIsVisible(!isVisible)}
          aria-label="Afficher ou cacher le mot de passe"
        />
      </InputRightElement>
    </InputGroup>
  );
});

PasswordInput.displayName = "PasswordInput";

export default PasswordInput;
