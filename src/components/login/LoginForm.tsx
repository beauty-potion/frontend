import {
  Alert,
  AlertDescription,
  AlertIcon,
  Button,
  FormControl,
  FormErrorMessage,
  Input,
  Stack,
} from "@chakra-ui/react";
import { useState } from "react";
import { useForm } from "react-hook-form";
import PasswordInput from "./PasswordInput";
import { LoginType, SignInResponse } from "@/schemas/LoginSchema";

type LoginFormProps = {
  onSubmit: (loginData: LoginType) => Promise<SignInResponse>;
};

const LoginForm = ({ onSubmit }: LoginFormProps) => {
  const {
    handleSubmit,
    register,
    formState: { errors },
  } = useForm<LoginType>();

  const [isSubmiting, setIsSubmiting] = useState(false);
  const [unknownErrorHappened, setUnknownErrorHappened] = useState(false);
  const [wrongEmailOrPassword, setWrongEmailOrPassword] = useState(false);

  async function submit(data: LoginType) {
    setIsSubmiting(true);
    setWrongEmailOrPassword(false);
    const response = await onSubmit(data);
    setWrongEmailOrPassword(response == SignInResponse.wrongEmailOrPassword);
    setUnknownErrorHappened(response == SignInResponse.unknownError);
    setIsSubmiting(false);
  }

  return (
    <form onSubmit={handleSubmit(submit)}>
      <Stack>
        <FormControl isInvalid={!!errors.email}>
          <Input {...register("email")} placeholder="Email" />
          <FormErrorMessage>Email invalide</FormErrorMessage>
        </FormControl>
        <FormControl>
          <PasswordInput {...register("password")} placeholder="Mot de passe" />
        </FormControl>
        <Button type="submit" colorScheme="primary" isLoading={isSubmiting}>
          Se connecter
        </Button>
        {unknownErrorHappened && (
          <Alert status="error">
            <AlertIcon />
            <AlertDescription>
              Une erreur est survenue, merci de réessayer plus tard.
            </AlertDescription>
          </Alert>
        )}
        {wrongEmailOrPassword && (
          <Alert status="error">
            <AlertIcon />
            <AlertDescription>
              L&apos;email et/ou le mot de passe saisi sont incorrects.
            </AlertDescription>
          </Alert>
        )}
      </Stack>
    </form>
  );
};

export default LoginForm;
