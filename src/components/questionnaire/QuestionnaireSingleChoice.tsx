import { Wrap } from "@chakra-ui/react";
import QuestionnaireChoiceCard from "./QuestionnaireChoiceCard";
import QuestionnaireStepWrapper, {
  QuestionnaireStepWrapperProps,
} from "./QuestionnaireStepWrapper";

type QuestionnaireSingleChoiceProps<
  DataType,
  Field extends keyof DataType,
  ValueType extends DataType[Field]
> = {
  field: Field;
  choices: {
    value: ValueType;
    title: string;
    subtitle?: string;
  }[];
  currentValue?: Partial<DataType>;
  onChange?: (field: Record<Field, ValueType>) => void;
} & QuestionnaireStepWrapperProps<ValueType>;

const QuestionnaireSingleChoice = <
  DataType,
  Field extends keyof DataType,
  ValueType extends DataType[Field]
>({
  field,
  choices,
  currentValue,
  onChange = () => null,
  ...wrapperProps
}: QuestionnaireSingleChoiceProps<DataType, Field, ValueType>) => (
  <QuestionnaireStepWrapper {...wrapperProps}>
    <Wrap gap={5} mt={5} justify="center">
      {...choices.map(({ value, title, subtitle }) => (
        <QuestionnaireChoiceCard
          key={JSON.stringify(value)}
          title={title}
          subtitle={subtitle}
          isSelected={value === currentValue?.[field]}
          onClick={() =>
            onChange({ [field]: value } as Record<Field, ValueType>)
          }
        />
      ))}
    </Wrap>
  </QuestionnaireStepWrapper>
);

export default QuestionnaireSingleChoice;
