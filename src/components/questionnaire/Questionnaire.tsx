import { Box } from "@chakra-ui/react";
import { ReactElement, cloneElement } from "react";

type QuestionnaireProps<DataType> = {
  steps: ReactElement[];
  currentStepIndex: number;
  currentValue: Partial<DataType>;
  onChange: (fields: Partial<DataType>) => void;
};

const Questionnaire = <DataType,>({
  steps,
  currentStepIndex,
  currentValue,
  onChange,
}: QuestionnaireProps<DataType>) => {
  const currentStepComponent = steps[currentStepIndex];

  return (
    <Box>
      {cloneElement(currentStepComponent, {
        ...currentStepComponent.props,
        currentValue,
        onChange,
      })}
    </Box>
  );
};

export default Questionnaire;
