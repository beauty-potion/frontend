import { Card, CardBody, CardHeader, Heading } from "@chakra-ui/react";

type QuestionnaireChoiceCardProps = {
  title: string;
  subtitle?: string;
  isSelected: boolean;
  onClick: () => void;
};

const QuestionnaireChoiceCard = ({
  title,
  subtitle,
  isSelected,
  onClick,
}: QuestionnaireChoiceCardProps) => (
  <Card
    width="md"
    height="s"
    _hover={{ backgroundColor: "primary.200", cursor: "pointer" }}
    {...(isSelected && { backgroundColor: "primary.500", color: "white" })}
    onClick={onClick}
  >
    <CardHeader>
      <Heading size="lg" textAlign="center">
        {title}
      </Heading>
    </CardHeader>
    {subtitle && (
      <CardBody textStyle={isSelected ? "base" : "comment"} textAlign="center">
        {subtitle}
      </CardBody>
    )}
  </Card>
);

export default QuestionnaireChoiceCard;
