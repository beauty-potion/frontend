import { Box, Button, Heading, Text, VStack } from "@chakra-ui/react";
import { PropsWithChildren, ReactElement } from "react";

type QuestionnaireStepWrapperProps<ValueType> = {
  question: string;
};

const QuestionnaireStepWrapper = <ValueType,>({
  question,
  children,
}: QuestionnaireStepWrapperProps<ValueType> & PropsWithChildren) => {
  return (
    <Box>
      <Heading textAlign="center">{question}</Heading>
      {children}
    </Box>
  );
};

export type { QuestionnaireStepWrapperProps };
export default QuestionnaireStepWrapper;
