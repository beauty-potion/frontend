import useAuth from "@/hooks/UseAuth";
import {
  Button,
  Flex,
  Heading,
  Image,
  LinkBox,
  LinkOverlay,
} from "@chakra-ui/react";

import NextLink from "next/link";
import { useRouter } from "next/router";
import UserButton from "./UserButton";

const loginPath = "/login";

const Header = () => {
  const { user, signOut } = useAuth();
  const { pathname } = useRouter();
  const isOnLoginPage = pathname == loginPath;

  const AuthButton = () =>
    !!user ? (
      <UserButton userEmail={user.email} onClickOnLogout={signOut} />
    ) : (
      <NextLink href={loginPath} passHref>
        <Button colorScheme="primary">Se connecter</Button>
      </NextLink>
    );

  return (
    <Flex
      p={2}
      h={16}
      backgroundColor="secondary.500"
      justifyContent="space-between"
      alignItems="center"
    >
      <LinkBox display="flex" gap={1} alignItems="end">
        <Image src="/logo.svg" alt="Beauty potion logo" />
        <Heading as="h1" size="lg">
          <LinkOverlay as={NextLink} href="/">
            Beauty Potion
          </LinkOverlay>
        </Heading>
      </LinkBox>
      {!isOnLoginPage && <AuthButton />}
    </Flex>
  );
};

export default Header;
