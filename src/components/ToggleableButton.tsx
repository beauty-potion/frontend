import { Button, ButtonProps } from "@chakra-ui/react";

type ToggleableButtonProps = {
  isToggled: boolean;
} & ButtonProps;

const ToggleableButton = ({
  isToggled,
  ...buttonProps
}: ToggleableButtonProps) => (
  <Button variant={isToggled ? "solid" : "outline"} {...buttonProps} />
);

export default ToggleableButton;
