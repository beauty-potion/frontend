import {
  Button,
  HStack,
  Icon,
  IconButton,
  Popover,
  PopoverArrow,
  PopoverBody,
  PopoverContent,
  PopoverTrigger,
  Text,
} from "@chakra-ui/react";
import { useState } from "react";
import { MdAccountCircle, MdEmail } from "react-icons/md";

type UserButtonProps = {
  userEmail: string;
  onClickOnLogout: () => Promise<void>;
};

const UserButton = ({ userEmail, onClickOnLogout }: UserButtonProps) => {
  const [isSigningOut, setIsSigningOut] = useState(false);
  function handleClickOnSignOut() {
    setIsSigningOut(true);
    onClickOnLogout().then(() => setIsSigningOut(false));
  }

  return (
    <Popover>
      <PopoverTrigger>
        <IconButton
          aria-label={"Utilisateur"}
          variant="ghost"
          size="md"
          icon={<MdAccountCircle size="100%" />}
        />
      </PopoverTrigger>
      <PopoverContent>
        <PopoverArrow />
        <PopoverBody>
          <HStack align="center">
            <Icon as={MdEmail} />
            <Text>{userEmail}</Text>
            <Button
              size="sm"
              onClick={handleClickOnSignOut}
              ml="auto"
              disabled={isSigningOut}
            >
              Se déconnecter
            </Button>
          </HStack>
        </PopoverBody>
      </PopoverContent>
    </Popover>
  );
};

export default UserButton;
