'use client'
import { Alert, AlertIcon, Text } from "@chakra-ui/react";
import { Link } from '@chakra-ui/next-js'
const ErrorAlert = () => {
  return (
    <Alert status="error">
      <AlertIcon />
      Une erreur est survenue, réssayez plus tard ou&nbsp;
      <Link href="/">
        retournez à la page d&apos;accueil
      </Link>
    </Alert>
  );
};

export default ErrorAlert;
