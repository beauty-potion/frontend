import { VStack } from "@chakra-ui/react";
import { ProduitCardProps } from "./ProduitCard";
import RoutineContainer from "./RoutineContainer";

type RoutinesViewProps = {
  matin: ProduitCardProps[];
};

const RoutinesView = ({ matin }: RoutinesViewProps) => (
  <VStack>
    <RoutineContainer title="Matin" produits={matin} />
  </VStack>
);

export default RoutinesView;
