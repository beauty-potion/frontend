import { Box, Heading, SimpleGrid } from "@chakra-ui/react";
import ProduitCard, { ProduitCardProps } from "./ProduitCard";

type RoutineContainerProps = {
  title: string;
  produits: ProduitCardProps[];
};

const RoutineContainer = ({ title, produits }: RoutineContainerProps) => (
  <Box width="100%">
    <Box backgroundColor="secondary.500">
      <Heading>{title}</Heading>
    </Box>
    <SimpleGrid minChildWidth="xs" gap={2} my={2}>
      {produits.map(({ slug, name, brand, imageUrl }) => (
        <ProduitCard
          key={slug}
          slug={slug}
          name={name}
          brand={brand}
          imageUrl={imageUrl}
        />
      ))}
    </SimpleGrid>
  </Box>
);

export default RoutineContainer;
