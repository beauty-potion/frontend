import { ButtonGroup } from "@chakra-ui/react";
import ToggleableButton from "../ToggleableButton";

type View = "routines" | "produits";
type ViewSwitcherProps = {
  value: View;
  onChange: (value: View) => void;
};

const ViewSwitcher = ({ value, onChange: onChange }: ViewSwitcherProps) => (
  <ButtonGroup isAttached>
    <ToggleableButton
      colorScheme="primary"
      isToggled={value == "routines"}
      onClick={() => onChange("routines")}
    >
      Routines
    </ToggleableButton>
    <ToggleableButton
      colorScheme="primary"
      isToggled={value == "produits"}
      onClick={() => onChange("produits")}
    >
      Produits
    </ToggleableButton>
  </ButtonGroup>
);

export default ViewSwitcher;
export type { View };
