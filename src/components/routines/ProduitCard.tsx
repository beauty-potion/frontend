import { Card, CardBody, Center, Heading, Img, Text } from "@chakra-ui/react";

type ProduitCardProps = {
  slug: string;
  name: string;
  brand: string;
  imageUrl: string;
};

const ProduitCard = ({ name, brand, imageUrl }: ProduitCardProps) => (
  <Card>
    <CardBody textAlign="center">
      <Center>
        <Img
          width="3xs"
          height="3xs"
          src={`${process.env.NEXT_PUBLIC_IMAGES_BUCKET}/${imageUrl}`}
          objectFit="contain"
        />
      </Center>
      <Text fontSize="lg" fontWeight="medium">
        {name}
      </Text>
      <Text textStyle="comment">{brand}</Text>
    </CardBody>
  </Card>
);

export default ProduitCard;
export type { ProduitCardProps };
