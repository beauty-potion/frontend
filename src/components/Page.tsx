import { Box, BoxProps, Container } from "@chakra-ui/react";
import { PropsWithChildren } from "react";
import Header from "./Header";

const Page = ({ children, ...boxProps }: PropsWithChildren & BoxProps) => {
  return (
    <Box minHeight="100vh" {...boxProps}>
      <Header />
      <Container
        maxWidth={{
          base: "container.sm",
          md: "container.lg",
        }}
        padding={5}
      >
        {children}
      </Container>
    </Box>
  );
};

export default Page;
