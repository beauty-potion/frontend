import { object, string, z } from "zod";

const passwordMinLength = 8;
enum StrongPasswordValidationErrors {
  notLongEnough = `Password must be at least ${passwordMinLength} characters long`,
  doesntContainLowercase = "Password must contain a lowercase",
  doesntContainUppercase = "Password must contain an uppercase",
  doesntContainNumber = "Password must contain a number",
}

const StrongPassword = string()
  .min(8, { message: StrongPasswordValidationErrors.notLongEnough })
  .regex(
    new RegExp("[a-z]"),
    StrongPasswordValidationErrors.doesntContainLowercase
  )
  .regex(
    new RegExp("[A-Z]"),
    StrongPasswordValidationErrors.doesntContainUppercase
  )
  .regex(
    new RegExp("[0-9]"),
    StrongPasswordValidationErrors.doesntContainNumber
  );

const LoginSchema = object({
  email: string().email(),
  password: StrongPassword,
});

type LoginType = z.infer<typeof LoginSchema>;

enum SignUpResponse {
  emailAlreadyExists,
  unknownError,
  success,
}

enum SignInResponse {
  wrongEmailOrPassword,
  unknownError,
  success,
}

export {
  LoginSchema,
  SignInResponse,
  SignUpResponse,
  StrongPasswordValidationErrors,
};

export type { LoginType };
